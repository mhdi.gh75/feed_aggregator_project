# FeedAggregator Project
This project is divided in three parts:
1. Gateway 
2. Users
3. Feeds

# Gateway
This part has two efficient role:
1. To Route the requests to the other services via rpc
2. To authenticate users and be as authentication layer

# Users
This part contains service, user, models and schema

**service**: Gets rpc request from gateway and pass the data to the schema for validation then pass the data to the user class which controlls the data

**user**: Base controller for user service

**schema**: Validator and serializer layer

**models**: Defines the Tables for the server (Data layer).

# Feeds
This part contains service, feed, feeditem, models and schema

**service**: Gets rpc request from gateway and pass the data to the schema for validation then pass the data to the feed and feeditem class which controlls the data

**feed**,**feeditem**: Base controller for feeds service

**schema**: Validator and serializer layer

**models**: Defines the Tables for the server (Data layer).


## Why nameko?

In this project i prefered to use nameko to have a microservice structure for project.
I tried to implement services in a loosely coupled structure.

## Run

At first we need to build the images for the containers.
so firstly run:
```
sudo -s
docker-compose build
docker-compose up
```

I had some issues with alembic migrations and i could'nt find the best way to solve it.
so i wrote a script which should be run to add the migrations .

run the command when the containers are fully started and the database is created.

`sudo ./run.sh`

Now the services are ready.

## Testing the services

Tests are in each service and they are written via pytest
for users part you can test the logic with commands below :
```
cd Users/
pytest test/test.py -vv -s
```

for feeds part you can test the logic with commands below :
```
cd Feeds/
pytest test/test.py -vv -s
```

The Gateway tests are not written because all requests are in these two part and the authentication check can be done via the API's which are in the postman document.

## Test via postman document

All the API's are placed in the postman json document.
you can import the document to test the services connection too.


For updating feeds i used nameko async call instead of other choices because the whole structure was based on nameko and i prefered to use that for this part too.
For the role back strategy i could'nt do it in time so i prefered not to add this part in project.
