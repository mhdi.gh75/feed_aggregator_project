"""create feeds table

Revision ID: dfb2474660f8
Revises: 
Create Date: 2021-09-21 13:12:15.973443

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dfb2474660f8'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'feeds',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('category', sa.String(100)),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('created_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime)
    )
    op.create_table(
        'feed_items',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column(
            'feed',
            sa.Integer,
            sa.ForeignKey(
                'feeds.id', ondelete='CASCADE', onupdate='CASCADE',
            )),
        sa.Column('title', sa.String(50), nullable=False),
        sa.Column('description', sa.String(500), nullable=False),
        sa.Column('created_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime)
    )
    op.create_table(
        'followed_feeds',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column(
            'feed',
            sa.Integer,
            sa.ForeignKey(
                'feeds.id', ondelete='CASCADE', onupdate='CASCADE',
            )),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('created_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime))
    
    op.create_table(
        'bookmarks',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column(
            'item',
            sa.Integer,
            sa.ForeignKey(
                'feed_items.id', ondelete='CASCADE', onupdate='CASCADE',
            )),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('created_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime)
    )


def downgrade():
    pass
