
from sqlalchemy.sql.functions import user
from feeds.models import DeclarativeBase
import pytest
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from nameko.rpc import RpcProxy, rpc
from nameko.testing.services import worker_factory
from feeds.service import FeedsService

import coloredlogs, logging
coloredlogs.install()

@pytest.fixture
def session():
    """ Create a test database and session
    """
    engine = create_engine('sqlite:///:memory:')
    DeclarativeBase.metadata.create_all(engine)
    session_cls = sessionmaker(bind=engine)
    return session_cls()


def test_feeds_service(session):
    # create worker with mock dependencies
    service = worker_factory(FeedsService, db=session)
    
    ''' 
    User service and feed service dont have foreign relation shio with each other
    They use nosql approach in sqlbased db
    Then i just use user_id as an static id for test here
    '''
    user_id = 1

    data = service.create(
        {
            "user_id": user_id,
            'name': 'FunFeed',
            'category': 'Fun'
        }
    )
    
    feed_id = data['data'][0]['id']

    data = service.list()
    logging.info('First Feed is created')
    logging.info('First feed data is: {}'.format(data))
    logging.info('-----------------------------------------------')

    updated_feed = service.update_feed(
        {
            'id': feed_id,
            'name': 'TestFunFeed',
            'category': 'StillFun'
        }
    )

    logging.info('First Feed is updated')
    logging.info('updated feed data is: {}'.format(updated_feed))
    logging.info('-----------------------------------------------')

    first_feed = service.get(feed_id)
    logging.info('Get First feed')
    logging.info('First feed data is: {}'.format(first_feed))
    logging.info('-----------------------------------------------')

    follow_feed = service.follow(user_id, feed_id)
    logging.info('First feed is followed by user {}'.format(user_id))
    logging.info('is first feed followed : {}'.format(follow_feed))
    logging.info('-----------------------------------------------')

    first_item = service.add_item({'title': 'First Title', 'description': 'First Title description'}, feed_id)
    item_id = first_item['data'][0]['id']
    logging.info('First item is created')
    logging.info('First item data is: {}'.format(first_item))
    logging.info('-----------------------------------------------')

    second_item = service.add_item({'title': 'Second Title', 'description': 'Second Title description'}, feed_id)
    logging.info('Second item is created')
    logging.info('Second item data is: {}'.format(second_item))
    logging.info('-----------------------------------------------')

    all_items = service.get_items(feed_id)
    logging.info('Get All items')
    logging.info('Items are : {}'.format(all_items))
    logging.info('-----------------------------------------------')
    

    updated_item = service.update_item({
        'id': item_id,
        'title': 'Updated Item',
        'description': 'This is an updated item',
        'feed': feed_id
    })
    logging.info('First item is updated')
    logging.info('Feeds items are : {}'.format(updated_item))
    logging.info('-----------------------------------------------')
    

    bookmark = service.add_bookmark(item_id,user_id)
    logging.info('Add first item to bookmark')
    logging.info('Is added to bookmark: {}'.format(bookmark))
    logging.info('-----------------------------------------------')
    

    all_bookmarks = service.get_bookmarks(user_id)
    logging.info('Get All users bookmark')
    logging.info('Users bookmark are: {}'.format(all_bookmarks))
    logging.info('-----------------------------------------------')
    