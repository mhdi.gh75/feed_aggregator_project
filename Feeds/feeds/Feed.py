
from sqlalchemy.sql.functions import user
from .models import FeedModel, FollowedFeedModel
from .schema import FeedSchema

class FeedClass:
    def __init__(self, db):
        self.db = db
    
    def add_feed(self, name, category, user_id):
        feed = FeedModel(name=name, category=category, user_id=user_id)
        self.db.add(feed)
        self.db.commit()

        return True

    def get_feeds(self):
        feeds = self.db.query(FeedModel).all()
        
        final_result = {
            'data': []
        }

        for obj in feeds:
            data = FeedSchema().dump(obj)[0]
            final_result['data'].append(data)

        return final_result
    
    def get_feed(self, feed_id):
        feed = self.db.query(FeedModel).get(feed_id)
        data = FeedSchema().dump(feed).data

        return data
    
    def update_feed(self, name, category, feed_id):
        feed = self.db.query(FeedModel).get(feed_id)
        feed.name = name
        feed.category = category
        self.db.commit()

        return True
    
    def follow_feed(self, user_id, feed_id):
        feed = self.db.query(FeedModel).get(feed_id)
        followed_feed = FollowedFeedModel(user_id=user_id,feed=feed.id)
        self.db.add(followed_feed)
        self.db.commit()

        return True