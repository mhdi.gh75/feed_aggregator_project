from sqlalchemy.sql.functions import user
from .models import *
from .schema import ItemSchema

class FeedItemClass:
    def __init__(self, db):
        self.db = db
    
    def add_item(self, title, description, feed):
        item = FeedItemModel(title=title, description=description, feed=feed)
        self.db.add(item)
        self.db.commit()

        return True
    
    def get_items(self, feed):
        
        item_query = self.db.query(FeedItemModel).filter_by(feed=feed)

        final_result = {
            'data': []
        }

        for obj in item_query:
            data = ItemSchema().dump(obj)[0]
            final_result['data'].append(data)

        return final_result
    

    def update_item(self, title, description, item_id):
        item = self.db.query(FeedItemModel).get(item_id)
        item.title = title
        item.description = description
        self.db.commit()

        return True
    
    def add_bookmark(self, item_id, user_id):
        bookmark_object = BookmarkedItemsModel(item=item_id, user_id=user_id)
        self.db.add(bookmark_object)
        self.db.commit()

        return True
    

    def get_bookmarks(self, user_id):
        items = self.db.query(FeedItemModel,BookmarkedItemsModel).filter(BookmarkedItemsModel.user_id==user_id).filter(FeedItemModel.id==BookmarkedItemsModel.item).all()
        

        return items