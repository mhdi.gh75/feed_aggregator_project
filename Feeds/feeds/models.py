import datetime

from sqlalchemy import (
    DECIMAL, Column, DateTime, ForeignKey, Integer, UniqueConstraint
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import String


class Base(object):
    created_at = Column(
        DateTime,
        default=datetime.datetime.now(),
        nullable=False
    )
    updated_at = Column(
        DateTime,
        default=datetime.datetime.now(),
        onupdate=datetime.datetime.now(),
        nullable=False
    )


DeclarativeBase = declarative_base(cls=Base)


class FeedModel(DeclarativeBase):
    __tablename__ = "feeds"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    category = Column(String, nullable=False)
    user_id = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)


class FollowedFeedModel(DeclarativeBase):
    __tablename__ = "followed_feeds"

    id = Column(Integer, primary_key=True, autoincrement=True)
    feed = Column(ForeignKey('feeds.id'), nullable=False)
    user_id = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    __table_args__ = (UniqueConstraint('user_id', 'feed', name='user_feed'),
                     )


class FeedItemModel(DeclarativeBase):
    __tablename__ = 'feed_items'

    id = Column(Integer, primary_key=True, autoincrement=True)
    feed = Column(ForeignKey('feeds.id'), nullable=False)
    title = Column(String(50), nullable=False)
    description = Column(String(500), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)



class BookmarkedItemsModel(DeclarativeBase):
    __tablename__ = 'bookmarks'

    id = Column(Integer, primary_key=True, autoincrement=True)
    item = Column(ForeignKey('feed_items.id'), nullable=False)
    user_id = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)