
import logging

from sqlalchemy.sql import base
import jwt
import os
import base64

from nameko.events import event_handler
from nameko.rpc import rpc

from .models import Base, FeedModel, DeclarativeBase
from .Feed import FeedClass
from .FeedItem import FeedItemClass, ItemSchema
from nameko_sqlalchemy import DatabaseSession
from passlib.hash import sha256_crypt
from werkzeug import Response

import json


class FeedsService:

    name = 'feeds'

    db = DatabaseSession(DeclarativeBase)

    @rpc
    def create(self, feed):
        user_id = feed['user_id']
        feed_object = FeedClass(self.db)
        feed_object.add_feed(feed['name'], feed['category'], user_id)

        feeds = feed_object.get_feeds()
        
        return feeds
    
    @rpc
    def list(self):
        feed_object = FeedClass(self.db)
        feeds = feed_object.get_feeds()
        
        return feeds
    
    @rpc
    def update_feed(self, feed):
        feed_object = FeedClass(self.db)
        feed_object.update_feed(feed['name'], feed['category'], feed['id'])

        feeds = feed_object.get_feeds()

        return feeds


    @rpc
    def get(self, feed_id):
        feed_object = FeedClass(self.db)
        feed = feed_object.get_feed(feed_id)
        
        return feed
    
    @rpc
    def follow(self, user_id, feed_id):
        feed_object = FeedClass(self.db)

        feed_object.follow_feed(user_id, feed_id)

        return True


    @rpc
    def add_item(self, item, feed_id):
        item_object = FeedItemClass(self.db)
        item_object.add_item(item['title'], item['description'], feed_id)

        items = item_object.get_items(feed_id)
        
        return items
    

    @rpc
    def get_items(self, feed_id):
        item_objects = FeedItemClass(self.db)
        items = item_objects.get_items(feed_id)

        return items

    @rpc
    def update_item(self, item):
        feed_id = item['feed']
        item_object = FeedItemClass(self.db)
        item_object.update_item(item['title'], item['description'], item['id'])

        items = item_object.get_items(feed_id)

        return items
    
    @rpc
    def add_bookmark(self, item, user_id):
        item_object = FeedItemClass(self.db)
        item_object.add_bookmark(item, user_id)

        return True
    
    @rpc
    def get_bookmarks(self, user_id):
        item_object = FeedItemClass(self.db)
        items = item_object.get_bookmarks(user_id)

        final_result = []

        for item in items:
            item = item[0]
            obj = {
                'title': item.title,
                'description': item.description,
                'id': item.id
            }
            final_result.append(obj)

        return final_result