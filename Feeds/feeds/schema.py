from marshmallow import Schema, fields


class FeedSchema(Schema):
    id = fields.Str(required=True)
    name = fields.Str(required=True)
    category = fields.Str(required=True)
    user_id = fields.Int(required=True)


class FollowedFeedSchme(Schema):
    feed = fields.Int(required=True)
    user_id = fields.Int(required=True)


class ItemSchema(Schema):
    id = fields.Int(required=True)
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    feed = fields.Int(required=True)