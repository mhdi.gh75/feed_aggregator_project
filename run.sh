#!/bin/sh
docker exec -it digi-project-feeds alembic upgrade head
docker exec -it digi-project-postgres su postgres -c  "psql -d digiproject -c 'delete from alembic_version;'";
docker exec -it digi-project-users alembic upgrade head

