
from passlib.hash import sha256_crypt
import jwt
import re
import base64
from .models import UserModel, Base, DeclarativeBase
from .schema import UserSchema
import os
from nameko_sqlalchemy import DatabaseSession

#

class UserClass:
    def __init__(self, db):
        self.db = db
    
    def add_user(self, username, password, email):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if not re.fullmatch(regex, email):
            return False

        encoded_pwd = sha256_crypt.encrypt(password)

        user = UserModel(username=username, password=encoded_pwd, email=email)
        self.db.add(user)
        self.db.commit()


        user_data = UserSchema().dump(user).data
        user_data['id'] = user.id

        return user_data
    
    def get_user(self, user_id):
        user = self.db.query(UserModel).get(int(user_id))
        return UserSchema().dump(user).data
    
    