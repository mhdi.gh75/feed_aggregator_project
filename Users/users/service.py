
from sqlalchemy.sql import base
import jwt
import os
import base64

from nameko.events import event_handler
from nameko.rpc import rpc

from .models import Base, UserModel, DeclarativeBase
from nameko.events import EventDispatcher
from .Users import UserClass
from nameko_sqlalchemy import DatabaseSession
from passlib.hash import sha256_crypt
import json


class UsersService:

    name = 'users'

    db = DatabaseSession(DeclarativeBase)

    @rpc
    def create(self, user):
        user_object = UserClass(self.db)
        
        user_data = user_object.add_user(user['username'], user['password'], user['email'])
        
        encoded_jwt = jwt.encode({"user_id": user_data['id']}, str(os.getenv('SECRET_KEY')), algorithm="HS256")   

        return user_data
    
    @rpc
    def login(self, user):
        
        user_query = self.db.query(UserModel).filter_by(username=user['username']).first()
        
        if sha256_crypt.verify(user['password'],user_query.password):
                
            encoded_jwt = jwt.encode({"user_id": user_query.id}, str(os.getenv('SECRET_KEY')), algorithm="HS256")        

            return encoded_jwt
        
        else:
            return False

    @rpc
    def get_user(self, user):
        user_object = UserClass(self.db)

        user_data = user_object.get_user(user)

        return user_data
