"""Columns added

Revision ID: 5ae86b73edbe
Revises: 5285e2069da0
Create Date: 2021-09-21 02:26:56.625525

"""

# revision identifiers, used by Alembic.
revision = '5ae86b73edbe'
down_revision = '5285e2069da0'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('users', sa.Column('created_at', sa.DateTime))
    op.add_column('users', sa.Column('updated_at', sa.DateTime))


def downgrade():
    pass
