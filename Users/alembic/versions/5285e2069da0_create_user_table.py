"""create user table

Revision ID: 5285e2069da0
Revises: 
Create Date: 2021-09-20 17:23:13.699718

"""

# revision identifiers, used by Alembic.
revision = '5285e2069da0'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String(50), nullable=False),
        sa.Column('password', sa.String(100)),
        sa.Column('email', sa.String(50), nullable=False)
    )


def downgrade():
    pass
