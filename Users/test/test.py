from users.models import DeclarativeBase
import pytest
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from nameko.rpc import RpcProxy, rpc
from nameko.testing.services import worker_factory
from users.service import UsersService

import coloredlogs, logging
coloredlogs.install()


@pytest.fixture
def session():
    """ Create a test database and session
    """
    engine = create_engine('sqlite:///:memory:')
    DeclarativeBase.metadata.create_all(engine)
    session_cls = sessionmaker(bind=engine)
    return session_cls()


def test_users_service(session):
    # create worker with mock dependencies
    service = worker_factory(UsersService, db=session)

    data = service.create(
        {
            'username': 'FirstUser',
            'email': 'FirstUser@gmail.com',
            'password': '123456'
        }
    )

    logging.info('User created')
    logging.info('First User data: {}'.format(data))
    logging.info('-----------------------------------------------')


    user_id = data['id']
    token = service.login(
        {
            'username': 'FirstUser',
            'password': '123456'
        }
    )

    logging.info('User logged in')
    logging.info('User token is: {}'.format(token))
    logging.info('-----------------------------------------------')

    user_data = service.get_user(user_id)
    logging.info('Get user api is called')
    logging.info('User data is: {}'.format(user_data))
    logging.info('-----------------------------------------------')