from sqlalchemy.sql.functions import user
from nameko.rpc import rpc
from nameko.web.handlers import http
import jwt
import os
import json

from nameko.rpc import RpcProxy
from nameko.exceptions import BadRequest

from .schema import *

from werkzeug import Response

def authenticate(request):
    token = request.headers['Token']
    try:
        payload = jwt.decode(token, str(os.getenv('SECRET_KEY')), algorithms=["HS256"])
        return payload

        
    except Exception as e:
        return False

class GatewayService(object):
    """
    Service acts as a gateway to other services over http.
    """

    name = 'gateway'

    users_rpc = RpcProxy('users')
    feeds_rpc = RpcProxy('feeds')

    @http(
        "POST", "/users/sign-up"
    )
    def create_user(self, request):
        """ Create new user
        """

        schema = CreateUserSchema()

        try:
            user_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))

        # Add the User
        result = self.users_rpc.create(user_data)
        
        return Response(
            json.dumps(result), mimetype='application/json'
        )
    

    @http(
        "POST", "/users/login"
    )
    def login(self, request):
        """ user login
        """

        schema = LoginSchema()

        try:
            user_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))

        
        result = self.users_rpc.login(user_data)
        
        if result: return Response(
            json.dumps({'token': result}), mimetype='application/json'
        )

        else: return Response(
            json.dumps(
                {
                    'data': 'User not found'
                }
            ), mimetype='application/json'
        )

    @http(
        "GET", "/users/get-user/<int:user_id>"
    )
    def get_user(self, request, user_id):
        """ Add new feed
        """
        payload = authenticate(request)
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        user = self.feeds_rpc.get_user(user_id)
        
        return Response(json.dumps(user),
            mimetype='application/json'
        )

    
    @http(
        "POST", "/feeds/add-feed"
    )
    def create_feed(self, request):
        """ Add new feed
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        schema = CreateFeedSchema()

        try:
            feed_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))

        feed_data['user_id'] = user_id        
        self.feeds_rpc.create.call_async(feed_data)

        
        return Response(
            json.dumps({'data': 'Feed added'}), mimetype='application/json'
        )

    @http(
        "GET", "/feeds/get-feeds"
    )
    def get_feeds(self, request):
        """ Add new feed
        """
        payload = authenticate(request)
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        feed = self.feeds_rpc.list()
        
        return Response(json.dumps(feed),
            mimetype='application/json'
        )
    
    
    @http(
        "PATCH", "/feeds/update-feed/"
    )
    def update_feed(self, request):
        """ update a feed
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        schema = UpdateFeedSchema()

        try:
            feed_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))
        
        self.feeds_rpc.update_feed.call_async(feed_data)

        return Response(
            json.dumps({'data': 'feed updated'}), mimetype='application/json'
        )




    @http(
        "GET", "/feeds/get-feed/<int:feed_id>"
    )
    def get_feed(self, request, feed_id):
        """ Get a feed
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        feed = self.feeds_rpc.get(feed_id)
        return Response(
            GetFeedSchema().dumps(feed).data,
            mimetype='application/json'
        )
    

    @http(
        "POST", "/feeds/follow-feed/<int:feed_id>"
    )
    def follow_feed(self, request, feed_id):
        """ Follow feed
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        feed = self.feeds_rpc.follow(user_id, feed_id)
        return Response(json.dumps(
            {
                'data': 'Feed Followed'
            }),
            mimetype='application/json'
        )

    @http(
        "POST", "/feeds/add-item/<int:feed_id>"
    )
    def add_item(self, request, feed_id):
        """ Add new Item
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        schema = AddItemSchema()

        try:
            item_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))

        self.feeds_rpc.add_item.call_async(item_data, feed_id)

        return Response(json.dumps(
            {
                'data': 'Item added'
            }
        ),
            mimetype='application/json'
        )
    

    @http(
        "GET", "/feeds/get-items/<int:feed_id>"
    )
    def get_items(self, request, feed_id):
        """ Get a feed items
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        items = self.feeds_rpc.get_items(feed_id)
        return Response(
            json.dumps(items),
            mimetype='application/json'
        )



    @http(
        "PATCH", "/feeds/update-item/"
    )
    def update_item(self, request):
        """ update an item
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        schema = UpdateItemSchema()

        try:
            item_data = schema.loads(request.get_data(as_text=True)).data
            

        except ValueError as exc:
            raise BadRequest("Invalid json: {}".format(exc))
        
        self.feeds_rpc.update_item.call_async(item_data)
        
        return Response(
            json.dumps({'data': 'item updated'}), mimetype='application/json'
        )

    

    @http(
        "POST", "/feeds/add-bookmark/<int:item_id>"
    )
    def add_bookmark(self, request, item_id):
        """ Add item to bookmark
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        self.feeds_rpc.add_bookmark(item_id, user_id)
        return Response(json.dumps({
            'data': 'Item added to bookmark'
        }),
            mimetype='application/json'
        )
    

    @http(
        "GET", "/feeds/get-bookmarks/"
    )
    def get_bookmarks(self, request):
        """ Get bookmarks
        """
        payload = authenticate(request)
        user_id = ''
        if payload:
            user_id = payload['user_id']
        
        else:
            raise BadRequest("Invalid User")

        items = self.feeds_rpc.get_bookmarks(user_id)
        
        return Response(json.dumps(
            {'data': items}),
            mimetype='application/json'
        )