from re import S
from werkzeug import Request
from marshmallow import Schema, fields


class CreateUserSchema(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    email = fields.Str(required=True)


class LoginSchema(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)


class CreateFeedSchema(Schema):
    name = fields.Str(required=True)
    category = fields.Str(required=True)

class UpdateFeedSchema(Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    category = fields.Str(required=True)

class GetFeedSchema(Schema):
    name = fields.Str(required=True)
    category = fields.Str(required=True)


class AddItemSchema(Schema):
    title = fields.Str(required=True)
    description = fields.Str(required=True)


class GetItemSchema(Schema):
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    id = fields.Int(required=True)
    item = fields.Int(requires=True)

class UpdateItemSchema(Schema):
    id = fields.Int(required=True)
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    feed= fields.Int(required=True)

